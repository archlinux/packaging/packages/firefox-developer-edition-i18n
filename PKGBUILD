# Maintainer: Andrew Crerar <crerar@archlinux.org>
# Contributor: Jan Alexander Steffens (heftig) <heftig@archlinux.org>

pkgbase=firefox-developer-edition-i18n
pkgver=137.0b3
pkgrel=1
pkgdesc="Language pack for Firefox Developer Edition"
url="https://www.mozilla.org/firefox/developer"
arch=(any)
license=(MPL-2.0)

_url=https://archive.mozilla.org/pub/firefox/releases/$pkgver
source=(
  "firefox-$pkgver-SHA512SUMS::$_url/SHA512SUMS"
  "firefox-$pkgver-SHA512SUMS.asc::$_url/SHA512SUMS.asc"
)
validpgpkeys=(
  # Mozilla Software Releases <release@mozilla.com>
  # https://blog.mozilla.org/security/2023/05/11/updated-gpg-key-for-signing-firefox-releases/
  14F26682D0916CDD81E37B6D61B7B526D98F0353
)

_languages=(
  'ach         "Acholi"'
  'af          "Afrikaans"'
  'an          "Aragonese"'
  'ar          "Arabic"'
  'ast         "Asturian"'
  'az          "Azerbaijani"'
  'be          "Belarusian"'
  'bg          "Bulgarian"'
  'bn          "Bengali"'
  'br          "Breton"'
  'bs          "Bosnian"'
  'ca          "Catalan"'
  'ca-valencia "Catalan (Valencian)"'
  'cak         "Maya Kaqchikel"'
  'cs          "Czech"'
  'cy          "Welsh"'
  'da          "Danish"'
  'de          "German"'
  'dsb         "Lower Sorbian"'
  'el          "Greek"'
  'en-CA       "English (Canadian)"'
  'en-GB       "English (British)"'
  'en-US       "English (US)"'
  'eo          "Esperanto"'
  'es-AR       "Spanish (Argentina)"'
  'es-CL       "Spanish (Chile)"'
  'es-ES       "Spanish (Spain)"'
  'es-MX       "Spanish (Mexico)"'
  'et          "Estonian"'
  'eu          "Basque"'
  'fa          "Persian"'
  'ff          "Fulah"'
  'fi          "Finnish"'
  'fr          "French"'
  'fur         "Friulian"'
  'fy-NL       "Frisian"'
  'ga-IE       "Irish"'
  'gd          "Gaelic (Scotland)"'
  'gl          "Galician"'
  'gn          "Guarani"'
  'gu-IN       "Gujarati (India)"'
  'he          "Hebrew"'
  'hi-IN       "Hindi (India)"'
  'hr          "Croatian"'
  'hsb         "Upper Sorbian"'
  'hu          "Hungarian"'
  'hy-AM       "Armenian"'
  'ia          "Interlingua"'
  'id          "Indonesian"'
  'is          "Icelandic"'
  'it          "Italian"'
  'ja          "Japanese"'
  'ka          "Georgian"'
  'kab         "Kabyle"'
  'kk          "Kazakh"'
  'km          "Khmer"'
  'kn          "Kannada"'
  'ko          "Korean"'
  'lij         "Ligurian"'
  'lt          "Lithuanian"'
  'lv          "Latvian"'
  'mk          "Macedonian"'
  'mr          "Marathi"'
  'ms          "Malay"'
  'my          "Burmese"'
  'nb-NO       "Norwegian (Bokmål)"'
  'ne-NP       "Nepali"'
  'nl          "Dutch"'
  'nn-NO       "Norwegian (Nynorsk)"'
  'oc          "Occitan"'
  'pa-IN       "Punjabi (India)"'
  'pl          "Polish"'
  'pt-BR       "Portuguese (Brazilian)"'
  'pt-PT       "Portuguese (Portugal)"'
  'rm          "Romansh"'
  'ro          "Romanian"'
  'ru          "Russian"'
  'sat         "Santali"'
  'sc          "Sardinian"'
  'sco         "Scots"'
  'si          "Sinhala"'
  'sk          "Slovak"'
  'skr         "Saraiki"'
  'sl          "Slovenian"'
  'son         "Songhai"'
  'sq          "Albanian"'
  'sr          "Serbian"'
  'sv-SE       "Swedish"'
  'szl         "Silesian"'
  'ta          "Tamil"'
  'te          "Telugu"'
  'tg          "Tajik"'
  'th          "Thai"'
  'tl          "Tagalog"'
  'tr          "Turkish"'
  'trs         "Chicahuaxtla Triqui"'
  'uk          "Ukrainian"'
  'ur          "Urdu"'
  'uz          "Uzbek"'
  'vi          "Vietnamese"'
  'xh          "Xhosa"'
  'zh-CN       "Chinese (Simplified)"'
  'zh-TW       "Chinese (Traditional)"'
)

pkgname=()
noextract=()

for _lang in "${_languages[@]}"; do
  _locale=${_lang%% *}
  _pkgname=firefox-developer-edition-i18n-${_locale,,}
  _pkg=firefox-developer-edition-i18n-$pkgver-$_locale.xpi

  pkgname+=($_pkgname)
  source+=("$_pkg::$_url/linux-x86_64/xpi/$_locale.xpi")
  noextract+=($_pkg)
  eval "package_$_pkgname() {
    _package $_lang
  }"
done

verify() {
  cd "$SRCDEST"
  sed -n "s|  linux-x86_64/xpi/|  firefox-developer-edition-i18n-$pkgver-|p" \
    firefox-$pkgver-SHA512SUMS | sha512sum -c -
}

_package() {
  pkgdesc="$2 language pack for Firefox Developer Edition"
  depends=("firefox-developer-edition>=$pkgver")
  install -Dm644 firefox-developer-edition-i18n-$pkgver-$1.xpi \
    "$pkgdir/usr/lib/firefox-developer-edition/browser/extensions/langpack-$1@firefox.mozilla.org.xpi"
}

b2sums=('51efc9224494ad53b6d198d261d7f664a5c57dc1efb69bc698b6eebf38aad80f183d6d49da6ada214283d28fb14bcb49f8752b2e0afc990e0e0a2d5a56a64693'
        'SKIP'
        'fddd1f3e0c70cb51408b76249580df30083a421b50467efdb939d302de18aa902394d9c743b41cbbef040bcbbc248ad26ee83003602c67d5c9dca1aefb5346bf'
        '554c07ae5760b75cd8b857a7dc6ab8e520fe806a6ea514998aa92c390fa14bf6a8df269d1e6adf24c43f8658b107806dc1bd2cefe46bfc000e56d06bda6aedb4'
        '273338731515aa9675a8bff377dd3420138fdc15220ff9c0ae96a7c4ef675d1b50126d9a05c425fc3db62cd55437a076a86e5fb0f149d5257ee452edc0986b73'
        '3b05959b749c7f89dcd86908264a82c3fff6ead54091e7acde9ede7e983914b52242299550a610c60c2246d69eb6752283cc7532cc70970ce36f1715b047a1c4'
        'd83b8db51436b4b2504b10d1e4a68d65ce4003b5f97087f2e34996687e028695b4fdcf32d6e0e6d2a5e92d28540b7e16719e91af90cddbe0dc053c7c4238e1a2'
        '51efb567f23883e9ba7aa2ff3ec050315b61457584e6220324868d42536b65b88ee59e02da1f15a5d45a28a0994f3d9af9d5fb91a8f425c38a50857fa452a5cf'
        '0d6b3e7c391c78894b3dbb576e707008e2a14c53e0ade100fd13946a696c2d59dc25776a9b1f5e7fb55e017c9ce41345868ffbca38ee68eb45c62ad5cfd8f0cb'
        '2bc8321aa1ddab2a2094d75b68e28815e66f3f755739c00230131b5821b52c8c3f0bbdbc85531a1b39c2839ae69b17684bc282d5fefcbd0ad9eb646e99ccbbdc'
        '16f5d3ecc477c52decf881636f6a90f80a70dd16417878bc237da68968c5952436f414d81dbd8f366284097e9d327d534c38f86788f8c06c996981f63d5c7ce8'
        'e2e34a329cd18af5f5201c91d1a8d8c2bf33cca42a30962d46e8ac3477c4a9a4442e818b71cc3afea5af75c27bb4a4914fde0fdb54fd47ab50205797aceba2d3'
        '5efcff730bbd86684c1d59d2001e86b6c8fb29ee6b9c7be41da93f92ae7386e98ab9956a4c543a9d1562f36d3acfc561fbb97b046364142a776b8c4ae6750355'
        'f63841e1389cf0c9fc0dc3291025c2eedbb0bbaf7be585a8943a7703c49c728ffe0687691d5e394180b18de3c72535fc673e1260bc8405ad2b9871d0ed9709d7'
        '9a74aed71f1be6c69b5026ae54f7f4664ecb817023b7c40f2dfcf3b68887a5c146542a1a5bdb52b831fbf48e95985dad3170c13b6e867c0e8237e755139bbfb2'
        'bd9ffc765d26ab44cd4028a06741b0c9f748f18ca521c7dfa023b8cb71a753b39400257f5d985f6b0ee05b1eb51ae21f8230326beb8aabdc3e398da48143c71f'
        '546313207089f1b5ad4ff3cde6c583546521b542f3508bff43b85e8e8b85e4f3de317508c1de59a37d8d5817f344899e76a608aeb7cb391773130304db2b59b5'
        '575daf6aa2afb0a89a27ec673a5761eba2f0bddfbc5bcb039f6f2b17437ad66b566a04ebf42d9dbb08f8f65151cb811ba3025317a6b16370fbdf98a455688541'
        '83bb8da4834cf67ebf691fbd1629036eec561953b624401de617b91596d4335fa061e993bbe8ff8695917921e2ab01bf28809aeca06ab0402190a8d37f00915b'
        '256ee19c7654723c541560ea8f4783858f4852e7f1cf1130d53caa050eac8115a7b8f3ffc26df27327f717e16f68c5a9cc79cc7c1ae573065afd4fa97be46d60'
        '6e1252c22ec44ffb4624634146b9ae661399eaeaeaf7b6d43910b04e79db7b423d4f28e729ac72c0cdbd904dc3124c26e2d7739381f4f5adfa6d2cc5568a0361'
        '9ea568978ffaa9d3b87bc982108dbcfcbf5ce294a6ebb8f2484321817ea6707331170062019ca5150768036a3d8fa495d59d501b0a57f3a67181a842b7ba3563'
        '0de920783e23a427d3ec95a19780cfa05fac126cb4490d71f61fc3f61b15bd3145cbf9a7f2974ce063aa7d6db0363dbbbb617e587897548960b1af9efc4b91ed'
        '30b8f98e003ac7492899e53fa73b3e85d5cd77be90de101b6c604a0cf53f46f5c00363cdfd17ede1d514eae82d4e87c071e396b7585a80a840bd8aeceba4046d'
        '68ea793210bd18968178879bb218e4b77d33b09c38c1f3e470bc5846d34c1f5db848ede8e05fdc65c819b8cc60d0b8ba470b979318fe493fe3b35352cdecd2b6'
        '1449eb1ce20b6484b6d424a51c8ca2607a5051ace1a9eef23cd7dfdf6ebe9934f750ddc51740cb7f9d70132f081dcf8b7706b5c5bccd9b1270295bdb9e1243de'
        'c453fa6725bf2b4a48e9a5b463c59a932ac9c18a0a2080105a940fdc2049a0782bc61401f6664fc7a2f0e744efa335eeac18d452e379e490691688051337e184'
        '3538f4a3f353603f195763286bfe62cd173e216dcfd8c27d6a306baa2214cd6a408c99f881c83eb2a3753e670ca18236fe1229b368ba0f370db2b9a543e7d5ca'
        '947a80e81660ebc2b224d45d8fcbe8b86f37fae92745b5c382172a7769d5486e093befca228654fbb766eab93f249f7c7ddcbe8afa8a31e45aca355f688b848b'
        '5b13c428e255e59752e76259abb78b2975ba21279482a0602ae7071c08e0c836b4689f9941822e5165381b8d5de224c87341135a4527879e4c491817559663f9'
        '9d3cbd772a136495eb31ed3cd5f0a27f9a9ba596ca69f6337f194348a503211dca737f010adb81c085ad30b97389473e6ca8200327018370ea1069a83cc52247'
        'ff55d74d98f880cc8d6467c77df4c3a629786f3a80d1438515f9d433ea84c8adadd0d0b648993d14f099586268548ca531b81b1a3d9ec1c91b37c94e90753ff9'
        'ae50a59773b41ba24ac1880f81840884fb0ceb6a26939fd1824a003b39febe70d75a9a52eaef8a55194d95f457c1b210e1d80f3c38d8476678617b02751f74c7'
        '21a42d2af596d9c099c8c6131a18f0493b4e9cec1e45431196b8b8f8748bc49d0316aa72510681090ffda6645c1220f13338ad4647ac388d5262aee2d32362c9'
        'e73e6dc2c97bb7d25a7f50ef142ca9bfb030562a3ccd57fc9475457c776b990f23b49e1d68a65d39a8be3a04a6c0ac7723cb06cd389e2ba2263684bc16d66c09'
        '407c98b6464bf6338c4ff727f292289a84bdbf1dfef4b9b8a30681144331ac79441827f42b9bceb3f1357f2b3ca4f8747df81a7fb9625d275ba82a99e5635d85'
        '0839f109bce89e9e6aa9cfcdaed07f6dcb6be6caa1dc65ea52bb86a5c0b8feddf8b07de60eb493a86a6561556c8f5034d58496e8e7a3af7aae3fa98f262d0bf9'
        '19e1395d18463e449f54dc52efce68b990b34978f7d5e9a83c29ebd227f6cb013bdfe2b70317053d3657d436b62dd1e95a1a6e3bf5ff475e6aa8043508d6050a'
        '78cee4d12922033ee2be74eab5bea2160da2b17bba5ae6a23739f7c3724ef463f96c2c10fcf04a1d91ff82db5a4880459e238051786654ff8ceeb6a9f343180f'
        'fbeca1b04585b32d3f2485c3736b003aaa2e1fa804259e5d36deffeef503a90ad28d771d8233dbfe1a96005667210fe7119921c575fb725ea50ae8d6776c6bfc'
        'b5bb8d92afa46c3c773b4efff2f7d245797a4c06ab6da135a09245214d27750f0091fdcff6bc3e74433792ba921a60cee2d6768cc992b72cfaa502f6e7842f3a'
        'c65a54670f6636b062a3ce445f58c83d196455a7c7d117b7df6d1bac2bd254f9ff9e6e254930ca72a7ac1ac34f914fb9ef538b339dad841cea364713e0a6cec6'
        '0e309d4957a246c6886bd118c88b7d8a107e096c51cc5d2f49c7f07b61f70dde108858d335c4733d0764d4fdc5f06fe05bd4bd32dcd8cfd255767408c131df15'
        'ad856750335438a27df2e4e3d68b46d2a54863ffd2cb413346b0280cdf012d11272212002a4ea6712ca48208052cf3552da6019fd87472969dd069b030eb01f5'
        '95ba73b09527e8d42a0c30d13af81ae5ff5344f61c2514805d0db953356e6030e18ba912ea6894b56ae07cdbf4e05c0d80d9372731a2d8b713ba448084694ad9'
        '8ca97c002f4de0886fe25b032cf042e47f9e1c01bef8f2d3cfbb9536c6242c6b1962f817c01bce6379d0f465eb6754fc24cbc0df2bfb9c1eda83eff0e1d88db9'
        'b6a08e06634ce4a0feaebffa2f46ce4269043142a7b2da957300cbb6b06a7a9ea87058572a021827766aa22c07699994e9765099aaac5008eb9603378684a46a'
        'bc9cf0e6a21bc84499f1704c368fee1ce4dfc29be6cb0da67212e3fed20f929119c7aadb5feb36779e40c5a4dca171ca50dd311cfe64db0b83fd2b65ab569702'
        'e845fb89d1b42ba8cb051561facecb90d4ec37ba15866e5f1da9da148f9e138a58c151b428ddf39f6cb8b6a67cb45194ba271728913e32dab46f3c1f7629901c'
        '47a88602417cd80f7441b0b62f5debea2b893e1612fd3fcc26fc3422779a7939686d067985ffa2efe5402d315c9395d572795f9fe1442029f364e81e70da44cd'
        '3f7fdbfd9e9c2a16f2cdb925ffc4112951a5828f1c5cac4f1f7e2aa89a40b1ce6eb2352718460093dd387d5f8006f9df2ac83628e77f88978e4f351d6477354f'
        'e87f13c639b9c10cef6632981a6c2d71b16bb72c6d0a1fcc4eba4fb3f4890d190dca8a284d5cf9df4e19335e4c4c7758b6bf82f3b7bbc50cd5d72a69abe5a4ce'
        '700f70391a671eab2354aea294f7b35a0324f2fc72ef01cb4022dfe2ac6d2342ba1e1239fbeeb5e2d8ad466826bf741fb5fb61a74fdad5418c324b0b1a018d44'
        '97a7e31db40b7138230073cb876d99a35640a7fe11574e10e2f302eb9e346d7bd7655d8663f4ce81943a26d670c2491565087cff1950fd2d33bf496f2baef3c4'
        '2b94a1e690f502f077cb3719f5d42db37ffaa95b5272e174a43c401c867f9ef605007b81fc33a61ba06e5e345e06e21adc61dc39c93f2900695410ec17451466'
        '5078125fcaf074a4f6f46f32196472b73fcd3406972e853460f7c9882543bd1d5cb0885e13e0a050c593a20bd9003acfb5681e5a0dce34a25e63d591a360fd1b'
        'da432480b5089c65deac3a531959fddcadc7ab19e03f32851f1e5aa588da450acc9ee38670a414b505ef38acd71acbd1c72975c4030d984a64dd60bcd22a4e64'
        '1c68150aff47e714d23f4d3f88ec837fca451c0e9c0115833fb336c3a35850f4e410e26526ed037a2dceb0a869012aa67b6dd3f2228b5337b06b2ee116d2a2a8'
        '874cd1251479e57ff26041fe837b89d69dbbc253c5372a712f6762b4f1fc457ee1ba72a899e65017dbe46b459d1b5012fa1e1a855c2de2bd56e82487d08b392d'
        '9cb7bf2a516632eeb88af903b02154b8064c246a13639faf878f0f56d33d046b03b9bfff38a120aae80fef21749921ef4e080e4fcf4799deaa8b84b74e0ff463'
        '9ae957aae852f1a881f26aad71b27f00f61b86fb0442c1ba9148da5a391c908d59b481c6099145be7f6185e2cbcabc7df9d0fb1b06725e0c0355152513059dd9'
        '584f4a357da275e80a5774aee0ca40c7597ade11a633bfaeed6a1e528be6ee88a1a69fdddd508499c97259f4d1b1ff0750fe80eae7edd00ca9b237499eff70fb'
        '3f571d743c6d11a99b20002c1112d20344dcae8072cd87150ba5c52d0dcd3ac72806cad0415806f51fb599b0ce9e7396c0ab32cba79e0c0b56936586df428702'
        '367d879708eb1554665c39862e29cb80b14241a0dd5dff78d41ee85e0a15205d4d1cc2ef7665cb9e8466cf968587aa0bbfb5945773a8bfe9dd8f806a76dfcf31'
        '117215a4b25670e9d96ff2ad7be1812e5fcad47ef4bf9016270c75858c3eb8a212d218798b5f96b4bb29190e1b887cb1db1f29747006095e0a0b23f539fb94c9'
        'c93ece65e7bd33531c095cff7d0f6563872ff582624514fc9e3fbcd8f28f86b2ea9d3253baa4f920518ab97bcacb5857521868d47782a10a9709b45e17d4092a'
        '07f2745b695b1a6119b11aafc38ce8123e9d7803adfffebc9f4580e1da07c138c3b9c1e26c05d0cdf947e05682e614f81d7456c86874324150909980ded44513'
        'bde37a3e6c4c5cf6cc516b27d9364c8a4fbcc857057b5105fe5436c2b18ff61e069568dbde0b003d1c8f8e319c2e8143ac4cc510b2b7f0f841fbbe78ccb60bb6'
        'dd29f660d41ad8bcd30fdaf31aa145492a3f2cd4f4428d70cc63fba790c7ce70d0bc99e246d29f18bb4cb354a05290e4da1d9ecd25eb630e18b83aa9ef89b292'
        '6066763f105426837bc5f48ddfb85f563dce4a32fb025ad0c5441c62ff3d2be59371c73e4cc3c98a7d72227d6c6ad7b75c373b1e3d98ad90fdf051381b488ccb'
        '61e6388bafeeb5946e37e13c02e3dff5097b77746d95c017fd5e061f5e1fbdd0cc074564d9d6ce88805f66199ab2b4ed072bfdfe4fbfde21451d125181569522'
        'e58b8a52b8bcd0f684173a77ac4e6c4260102b8046edbf529dfdbdb8dac9ff50c13808e21baed53a4bece0ec2758f329711505f334316f86c7578f92950368f7'
        '1183ec92b0bd617c2ef9400304d76ad787279d1017ae18c9840f706b7b0c9f110d93802b492d18bed5c7b8028761977a979ed847b663223dfb877551ee593540'
        'd06ecd4d0ea3c84cd4822c4ef8a4fab00ead709bf9e0ce06486948a881b5be2076de04ecfe1e1151f1c21d342ab7b67935fa1dce140746aa876c476ca18132b1'
        '5272d54e58a73a90667e6d2806ecf315b480f84f227b2617a5f2522c18dcbbaba2b3fb4609300ba0a6ba541ff46e88cfe6670d959e0cbe7fc5c59bf756238b09'
        '96ad7fb4a735dc75004deefde4f49a6dc6b447e28c6def99cdc2a099c0802f0509f6c47747e74a860ecda5b0161b605ff73a27fb81305b3103d3b5750edd2c9a'
        'f29599342d7ccd5000e60f813e5f2b3ec5e57e434e325317dcf8743c7c0bfe971a32790a0de509756565641daf65bc7c5bccb3eb44e579ca5df828ef1e4d54a5'
        'd7390955e26054d2431528758f0ed689b1b98daa60006dd4222abf037ad0353b0ad852810233193bb1e1554e05b6e6554f9853dd27dfd17e8052e665ffb01ede'
        '30cc3d0cd6b6507a51593a798646bf268855e1b4daa20726c39c1c058264cbc394c0faa2976a47703ad9fc050e82a4d7521c2f97390546cac8d99e1448e8fefb'
        '7360a17c1dd38318e2e5468035c7e3187d50d97dd3901f6eee496fec2b46af9da9db3775f3ef53bc7cf0958c18521438c15ad57140c42f0ebe1eff20589ad539'
        '224264cbae9fabf96961488db6fcf364c46daddc4e9f88f680be60fc2c758f37971bce5cff9bf73f386f2c4d4b799b0dca7f8d65a6f6daed132eb1a91c07e7d7'
        '3108a3e93305cfe7d24a63befef6a1b2d628736b240b5dbb25bff427de9358f15fa8ece9e091cdb919b7a096f6b14be83e93c77402308b2153a2f51cd41bb1f9'
        '176eb8029ee08d15c38657fb28a95389da5bda93bcbf72871b389d916db1de8f225960eed497afea9cb07ea21b90e7430c720a5375d0a08779f533a73de5c49c'
        '0f6026f5e50f3bf3eb557226ffb51f820b55fc27b77030efeee87430f30bd14c836877bf931aa253f932a06062f34332a28ade7d028c43774c1462339e7b42c4'
        '3f258dc85207df8b06e448dfe765cfcd57385457634f246dd432e2feda7ca0f5276dbdd058a6139de4df41d4b291128cf38e49c284fe4af329f683452359cd9f'
        'ea75c44a1109ec14403ce0944e0168cdd35ae01c0021e8076c68db10af7cb3703a38bc94e2107dc6d2fe2ef94d59a6814b4f106854e8e56fb549557adb608243'
        '347bedeb5a03f7af0375275a6ca03d9b880434b6dc47902f2b8482eb43ce149cafc4cb1080e78382e1cbed1b37d18c415a13739d4fcb17142950158a0018534e'
        '929811c981c3b89bb9eb253d8cbe683bee206e7e478c5305ea75ea594df52933e333ed119e5421050b0e6dd970008b03b29849fa028386216adb15711ff7be33'
        '6c585bb206fa8e79f4061b84ee9304004d49bdf01c4982e27bb7bdafd2ff7cf0052c6d381aaab96fedec74f48ad737566361bc62338b0d8a34e4eddbb52ec0ba'
        '2e9bc208abc09822125010a23c192c8f24b6258cbd31c8f52a256a38beb953546e77e0dfaccd9d6a4df36fbe3a2c95c7a86a04309d1e2c944afc4fc88108377f'
        'd8827aef410c76638c8eb7de2eedf857ab6d242239bc393e49fd4d3efd4ccef4f116e0cb6b9b2c0eabb8c2d6d41bcfb795950bb0375a390f07868e754c15b2e7'
        'b8e6572ee387aa2da38f16fe351521b67185395454382c55d60530e1cad9c6d868144253e94e1620719b31c9098ca65e1655c3eb1338efeb2bcd32d0fdc4dfa6'
        '145909c62be391a3e77a3320483558451e6779c55d87bf0daa7f692969e5da3763d39c473af8768c4b9f43df084f1442405a147eb4086993b728681cf15608bf'
        '9b59c30d311bcdd7c82fb0298a969145549038440fe288b5de2695959c8b5cb2054874c6027ccc4eb51f10af1c2d37616ab009c90c76cea74f37f4863cabf7a5'
        '9e9006f6841b47c3519b6db72003ee2815a358d9e4ab7dabe5f6c61bb4ca1fb731db9c3c607a8e5e9be82ec44046a5c1be3374bb119011ada02bb1d459dd435c'
        '2700fe745cf993f2daf9419241a8fc2f0f89349fc50f2c78bad8b72dc87323950b64674b64632229f93ebe17f9202fecf433c94c99ca8a054a2d243435c66773'
        '23b6a096cb305d58e42795a446c63bf3b012515b8cd1da5c4131cdac1792f2b65be96620e55b8101e9de52bdc35fd04b4141123938c9a89536ca1e20c927df5d'
        '6f4e7cbc49614640986430d44e3d695c0a9d23d08e8ef839434050b5889600af09194fe54ad9b4289c2b192e6b7e1a62990717547f68b6cf8b90c9b60232217b'
        '31b0dfc5675006b13c915c920bd3be017cb50876cc4fed94fefc0cd90a3e735b676261acec80e224aaf622d56b1eaf895a7b11f28cdc6e46733b841b05ffc630'
        'f60cf3b5cde33f0bc08b2280eff43207ded842f7d178372314bd897994561de7323fb004e6972581d38893a5158c1817a2511b0a1e4798bc1eb40c9fb0c01920'
        '94933614ec0d38baa1a38b69ad7d800197e2227d78a5de5113ad8cd08b3cfab2e7d1a822e45d986ff6d1baef980c030c82e3f557b40296beb99757fd7bd45d6b'
        '3316347e3f510258067d040f67fb76e36771a263d58d318a0f41f732d30e3a3025d9fccb462fe5a796ddfae76cfc136203b9f28cd9647c82e91e9975601af3e6'
        '08c028d116b553076a27748e78a638903cd6818d3f5e681d6be817bf3fd37a9fce2498d7dd210967a6c0c9e97d525ca6e08a5d35285fd0e106dde4caecd413b0'
        'a2fc6c57d51d13280ea27e46484df133be7966bb0c0a15dc6faa8bd9c2de1d6ea667b908e5acf814995892bc82262544454e54dc75cb0deb895b42464e82ec9d'
        '554ceb4664025176c409b694badfd931d66b6339b56e9a540b4ff462dfaea57bff06bdab783d132cdddc461bc270ee86b6fe8258505df0704555ac8a07aff3af')
